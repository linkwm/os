/**
 * Copyright &copy; 2015-2020 <a href="http://www.jeeplus.org/">JeePlus</a> All rights reserved.
 */
package com.linkwm.utils;

import java.util.List;

/**
 * $.ajax后需要接受的JSON
 * 
 * @author
 * 
 */
public class JFinalResultJson {

	private static boolean success = true;// 是否成功
	private static String errorCode = "-1";//错误代码
	private static String msg = "操作成功";// 提示信息
	private  List list;
	
	
	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {//向json中添加属性，在js中访问，请调用data.msg
		this.msg = msg;
	}


	public boolean isSuccess() {
		return success;
	}

	public void setSuccess(boolean success) {
		this.success = success;
	}
	

	public void setErrorCode(String errorCode) {
		this.errorCode = errorCode;
	}

	public String getErrorCode() {
		return errorCode;
	}
	/**
	 * @return the result
	 */

	
	public static JFinalResultJson erroBody(String erroInfo){
		JFinalResultJson j = new JFinalResultJson();
		j.setSuccess(false);
		j.setErrorCode("7");
		j.setMsg(erroInfo);
		return j;
		
	}


	/**
	 * @return the list
	 */
	public List getList() {
		return list;
	}

	/**
	 * @param list the list to set
	 */
	public void setList(List list) {
		this.list = list;
	}

	public static JFinalResultJson erroServer(){
		JFinalResultJson j = new JFinalResultJson();
		j.setSuccess(false);
		j.setErrorCode("9");
		j.setMsg("系统异常");
		return j;
		
	}

	/**
	 * 保存类型返回结果
	 * @param j
	 * @return
	 */
	public JFinalResultJson successResult(JFinalResultJson j){
		j.setSuccess(success);
		j.setErrorCode(errorCode);
		j.setMsg("成功");		
		return j;
		
	}
	/**
	 * 状态通知类返回结果
	 * @param j
	 * @return
	 */
	public static JFinalResultJson successResult(String msg){
		JFinalResultJson j = new JFinalResultJson();
		j.setSuccess(true);
		j.setErrorCode("-1");
		j.setMsg(msg);		
		return j;
		
	}
	/**
	 * 状态通知类返回结果
	 * @param j
	 * @return
	 */
	public static JFinalResultJson successResult(List list){
		JFinalResultJson j = new JFinalResultJson();
		j.setSuccess(true);
		j.setErrorCode("-1");
		j.setList(list);		
		return j;
		
	}
	
	public static JFinalResultJson erroBody(List list){
		JFinalResultJson j = new JFinalResultJson();
		j.setSuccess(false);
		j.setErrorCode("7");
		j.setList(list);
		return j;
		
	}
	
}
