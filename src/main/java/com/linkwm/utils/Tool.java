package com.linkwm.utils;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.http.HttpServletRequest;

import com.jfinal.plugin.activerecord.Model;

public class Tool {
	public static Integer cookTime = 60*60*12;
	
	/**
	 * 获取客户端IP
	 * 
	 * @param request
	 * @return
	 */
	public static String getIpAddr_gxs(HttpServletRequest request) {
		String ip = request.getHeader("x-forwarded-for");
		if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
			ip = request.getHeader("Proxy-Client-IP");
		}
		if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
			ip = request.getHeader("WL-Proxy-Client-IP");
		}
		if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
			ip = request.getRemoteAddr();
		}
		if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
			ip = request.getHeader("http_client_ip");
		}
		if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
			ip = request.getHeader("HTTP_X_FORWARDED_FOR");
		}
		// 如果是多级代理，那么取第一个ip为客户ip
		if (ip != null && ip.indexOf(",") != -1) {
			ip = ip.substring(ip.lastIndexOf(",") + 1, ip.length()).trim();
		}
		return ip;
	}

	public static void main(String[] args) {
//		String e = "17009878888";
//		System.out.println(matchesPhoneNumber(e));
		Integer yuan = gongjiao(80);
		System.out.println("原价："+yuan+" 刷卡："+yuan*0.5+" 学生卡："+yuan*0.25);
	}
	public static Integer gongjiao(Integer s){
		Integer money = 2;
		int i =0;
		if(s>=10){
			 i = ((s-10)/5+1)*1;
		}
		money = money+i;
		return money;
		
	}
	
	public static String get6Random() {
		String sRand = "";
		for (int i = 0; i < 6; i++) {
			//String rand=String.valueOf(random.nextInt(10));
			String rand = getRandomChar();
			sRand = sRand.concat(rand);
		}
		return sRand;
	}

	/***************************************************************************************************************************************************************************************************
	 * 
	 * 产生随机数字或字符
	 */
	public static String getRandomChar() {
		int index = (int) Math.round(Math.random() * 2);
		String randChar = "";
		index = 2;
		switch (index) {
			case 0://大写字符
			randChar = String.valueOf((char) Math.round(Math.random() * 25 + 65));
				break;
			case 1://小写字符
			randChar = String.valueOf((char) Math.round(Math.random() * 25 + 97));
				break;
			default://数字
			randChar = String.valueOf(Math.round(Math.random() * 9));
				break;
		}
		return randChar;
	}
	

}
