package com.linkwm.controller;

import java.io.File;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import org.apache.log4j.Logger;

import com.linkwm.utils.JFinalResultJson;
import com.jfinal.aop.Before;
import com.jfinal.core.Controller;
import com.jfinal.ext.kit.DateKit;
import com.jfinal.kit.JsonKit;
import com.jfinal.kit.Prop;
import com.jfinal.kit.PropKit;
import com.jfinal.upload.UploadFile;
import com.linkwm.interceptor.*;

/**
 * 本 demo 仅表达最为粗浅的 jfinal 用法，更为有价值的实用的企业级用法 详见 JFinal 俱乐部:
 * http://jfinal.com/club
 * 
 * IndexController
 */
public class IndexController extends Controller {
	private static Logger logger = Logger.getLogger(IndexController.class);

	static Prop p = PropKit.use("linkwm_img_config_dev.txt").appendIfExists("linkwm_img_config_dev.txt");

	public void index() {
		render("pic.html");
	}
	
	public void jsp() {
		renderJsp("../jsp/controller.jsp");
	}

	@Before({ OriginNameInterceptor.class })
	public void uploadFile() {
		try {
			
			List<Object> urlList = new ArrayList<Object>();
			String encoding = "utf-8";
			Integer maxPostSize = 1024 * 1024 * 20;
			boolean b = false;
			String uploadPath = "upload/"+DateKit.toStr(new Date(), "yyyy") + "/" + DateKit.toStr(new Date(), "MM");
			List<UploadFile>  uploadList = getFiles(uploadPath, maxPostSize, encoding);
			for (UploadFile picFile : uploadList) {
				HashMap<String, String> urlObject = new HashMap<String, String> ();
				// 文件对象
				String fileName = picFile.getFileName();
				String mimeType = picFile.getContentType();// 得到
				// 上传文件的MIME类型:audio/mpeg
				if (!"image/gif".equals(mimeType) && !"image/jpeg".equals(mimeType) && !"image/x-png".equals(mimeType)
						&& !"image/png".equals(mimeType)) {
					urlObject.put("errorCode", "3");
					urlObject.put("url", "");
					urlObject.put("msg", fileName+"类型不正确");
					urlList.add(urlObject);
				}else{
				
					String lastName = fileName.substring(fileName.lastIndexOf(".")); // 获取文件的后缀
					String newName = DateKit.toStr(new Date(), "yyyyMMddHHmmssSSS") + lastName;
					
					String filepath = getFile().getUploadPath();
					boolean bo = new File(filepath + "/" + fileName).renameTo(new File(filepath + "/" + newName));
					String url = p.get("HTTPURL") + "/" + uploadPath + "/" + newName;				
					urlObject.put("errorCode", "-1");
					urlObject.put("url", url);
					urlObject.put("msg", "成功");
					urlList.add(urlObject);
					b = true;					
				}
				
				if(b){
					logger.info(JsonKit.toJson(JFinalResultJson.successResult(urlList)));
					renderJson(JFinalResultJson.successResult(urlList));
					return;
				}else{
					logger.info(JsonKit.toJson(JFinalResultJson.erroBody(urlList)));
					renderJson(JFinalResultJson.erroBody(urlList));
					return;
				}
				
			}
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			logger.info(JsonKit.toJson(JFinalResultJson.erroServer()));
			renderJson(JFinalResultJson.erroServer());
			return;
		}
	}
}
