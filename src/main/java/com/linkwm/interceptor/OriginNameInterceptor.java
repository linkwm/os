package com.linkwm.interceptor;

import java.util.Enumeration;

import javax.servlet.http.HttpServletRequest;


import org.apache.log4j.Logger;

import com.linkwm.utils.Tool;
import com.jfinal.aop.Interceptor;
import com.jfinal.aop.Invocation;

public class OriginNameInterceptor implements Interceptor {
	private static Logger logger = Logger.getLogger(OriginNameInterceptor.class);

	/**
	 * 打印APP请求来源和请求参数
	 */
	
	public void intercept(Invocation oninv) {
		try {
			oninv.getController().getResponse().addHeader("Access-Control-Allow-Origin", "*");
			StringBuilder sb = new StringBuilder();
			HttpServletRequest request = oninv.getController().getRequest();
			Enumeration<String> e = request.getParameterNames();
			while (e.hasMoreElements()) {
				String name = e.nextElement();
				String[] values = request.getParameterValues(name);
				if (values.length == 1) {
					sb.append(name).append("=").append(values[0]);
				} else {
					sb.append(name).append("[]={");
					for (int i = 0; i < values.length; i++) {
						if (i > 0)
							sb.append(",");
						sb.append(values[i]);
					}
					sb.append("}");
				}
				sb.append("  ");
			}
			if (!sb.toString().equals("")) {
				logger.info("method: " + oninv.getActionKey() +"请求参数:" + sb);
			}
			logger.info("请求ip:"+Tool.getIpAddr_gxs(request));
			oninv.invoke();
		} catch (Exception e) { 
			logger.error("请求参数出错",e);
			oninv.invoke();
		}
	}

}
