# imgservice

#### 介绍
图片服务器 
1、支持跨域上传图片（可以一次提交多张图片），返回JSON数据其中不包含重要信息就是图片资源URL地址
2、支持ueditor 上传多图到远程图片服务器，并返回对应的URL， ueditor会自动解析预览
#### 软件架构
软件架构说明
jfinal

#### 使用说明

1. 启动项目后打开默认主页会有本地上传demo 
2. 1 支持ueditor图片上传时调用的接口是在ueditor.config.js中设置
        serverUrl: URL + "jsp/controller.jsp" 其中URL 为远程服务器地址
2. 2 修改 config.json 中的saveRootPath和imageUrlPrefix

2. 3 其他参数根据自己的实际情况进行配置
